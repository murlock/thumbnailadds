<?php
/* TODO INSERT LICENCE */

namespace OCA\ThumbnailAdds;

use OCP\Files\File;
use OCP\IImage;

class Preview implements \OCP\Preview\IProviderV2 {
	/**
	 * {@inheritDoc}
	 */
	public function getMimeType(): string {
		return '/application\/comicbook\+zip/';
    }

    private function generateThumbnail(string $file): ?IImage {
        $zp = new \ZipArchive;
        $flag = \ZipArchive::CREATE;
        /* only since PHP 7.3 */
        if (defined('\ZipArchive::RDONLY')) {
            $flag = \ZipArchive::CREATE;
        }
        $zp->open($file, $flag);

        $name = null;
        $size = 0;
        for($i = 0; $i < $zp->numFiles; $i++) {
            $stat = $zp->statIndex($i);
            /* skip empty files or directories */
            if ($stat['size'] == 0) {
                continue;
            }
            /* TODO we should stop only on first image, not on first file */
            $name = $stat['name'];
            $size = $stat['size'];
            break;
        }

		if (!$name) {
			return null;
		}

		$maxSizeForImages = \OC::$server->getConfig()->getSystemValue('preview_max_filesize_image', 50);
		if ($maxSizeForImages !== -1 && $size > ($maxSizeForImages * 1024 * 1024)) {
			return null;
		}

		$image = new \OC_Image();
		$image->loadFromData($zp->getFromName($name));
		$image->fixOrientation();

		if ($image->valid()) {
			$image->scaleDownToFit($maxX, $maxY);
			return $image;
		}
		return null;
    }

	/**
	 * {@inheritDoc}
	 */
	public function getThumbnail(File $file, int $maxX, int $maxY): ?IImage {
        $file_resource = $file->fopen('r');
        if (!is_resource($file_resource)) {
            return null;
        }
        $tmp_resource = tmpfile();
        if (!is_resource($tmp_resource)) {
            return null;
        }
        stream_copy_to_stream($file_resource, $tmp_resource);
        fclose($file_resource);
        $tmp_path = stream_get_meta_data($tmp_resource)['uri'];
        try {
            return $this->generateThumbnail($tmp_path);
        } catch (\Exception $exc) {
            return null;
        } finally {
            unlink($tmp_path);
        }
    }

    public function isAvailable(\OCP\Files\FileInfo $file): bool {
        return true;
    }
}
