<?php
/* TODO insert Licence */
namespace OCA\ThumbnailAdds\AppInfo;

use OCA\ThumbnailAdds\Preview;
use OCP\AppFramework\App;

class Application extends App
{
    private $appName = "thumbnailadds";

    public function __construct() {
        parent::__construct($this->appName);
    }

    public function register() {
        $this->registerProvider();
    }

    public function registerProvider() {
        $appName = $this->appName;
        $server = $this->getContainer()->getServer();
        $logger = $server->getLogger();
        $previewManager = $server->query('PreviewManager');
		$previewManager->registerProvider('/application\/comicbook\+zip/', function () use ($logger, $appName) {
            return new Preview($logger, $appName);
        });

        $previewManager->GetProviders();
    }
}
